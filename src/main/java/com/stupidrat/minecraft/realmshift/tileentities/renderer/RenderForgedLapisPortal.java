package com.stupidrat.minecraft.realmshift.tileentities.renderer;

import java.util.Random;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import com.mojang.blaze3d.systems.RenderSystem;
import com.stupidrat.minecraft.realmshift.RealmShiftMod;
import com.stupidrat.minecraft.realmshift.tileentities.ForgedLapisPortalEntity;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.texture.TextureManager;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.world.LightType;

public class RenderForgedLapisPortal extends BlockEntityRenderer<ForgedLapisPortalEntity> {
    private TextureManager textureManager;
    private LightmapTextureManager lightTextureManager;

    public RenderForgedLapisPortal(BlockEntityRenderDispatcher dispatcher) {
        super(dispatcher);
        MinecraftClient client = MinecraftClient.getInstance();
        this.textureManager = client.getTextureManager();
        this.lightTextureManager = client.gameRenderer.getLightmapTextureManager();
    }

    public static final Identifier forgedLapisTexture = new Identifier(RealmShiftMod.MODID, "textures/blocks/forged_lapis_lazuli.png");

    private static final float[] posX = { 0, 1, 1, 0 };
    private static final float[] posY = { 0, 0, 1, 1 };
    private float portalHeight;

    @Override
    public void render(ForgedLapisPortalEntity te, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        RenderSystem.pushMatrix();
        RenderSystem.loadIdentity();
        RenderSystem.multMatrix(matrices.peek().getModel());

        RenderSystem.activeTexture(GL13.GL_TEXTURE0);
        this.textureManager.bindTexture(forgedLapisTexture);
        this.lightTextureManager.enable();

        Tessellator tes = Tessellator.getInstance();
        BufferBuilder buf = tes.getBuffer();

        portalHeight = (float) (Math.sin(te.getWorld().getTime() * 0.1) * 50 + 100);

        RenderSystem.translated(0, 1, 0);
        RenderSystem.scaled(2, 1, 2);


        RenderSystem.disableLighting();

        GL11.glEnable(GL11.GL_STENCIL_TEST);
        GL11.glStencilFunc(GL11.GL_ALWAYS, 1, 0xff);
        GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_REPLACE);
        GL11.glStencilMask(0xFF);
        GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);

        RenderSystem.enableDepthTest();

        buf.reset();
        buf.begin(GL11.GL_QUADS, VertexFormats.POSITION);
        buf.vertex(0, 0, 0).next();
        buf.vertex(0, 0, 1).next();
        buf.vertex(1, 0, 1).next();
        buf.vertex(1, 0, 0).next();
        tes.draw();

        RenderSystem.disableDepthTest();

        GL11.glStencilFunc(GL11.GL_EQUAL, 1, 0xFF);
        GL11.glStencilMask(0x00);

        // RenderSystem.bindTexture(MinecraftClient.getInstance().getTextureManager().getTexture(forgedLapisTexture).getGlId());

        buf.reset();
        buf.begin(GL11.GL_QUADS, VertexFormats.POSITION_TEXTURE_COLOR_LIGHT);
        for (int i = 0; i < 4; i++) {
            Random rnd = new Random();
            int j = (i + 1) % 4;
            float pX0 = posX[i];
            float pX1 = posX[j];
            float pY0 = posY[i];
            float pY1 = posY[j];

            int lightX0 = te.getWorld().getLightLevel(LightType.BLOCK, te.getPos().add(0.5, 0.5, 0.5).add(pX0, 0, pY0)) << 4;
            int lightY0 = te.getWorld().getLightLevel(LightType.SKY,   te.getPos().add(0.5, 0.5, 0.5).add(pX0, 0, pY0)) << 4;

            int lightX1 = te.getWorld().getLightLevel(LightType.BLOCK, te.getPos().add(0.5, 0.5, 0.5).add(pX1, 0, pY1)) << 4;
            int lightY1 = te.getWorld().getLightLevel(LightType.SKY,   te.getPos().add(0.5, 0.5, 0.5).add(pX1, 0, pY1)) << 4;

            buf.vertex(pX0, 0            , pY0).texture(0, 0           ).color(1f, 1f, 1f, 1f).light(lightX0, lightY0).next();
            buf.vertex(pX0, -portalHeight, pY0).texture(0, portalHeight).color(0f, 0f, 0f, 0f).light(0      , 0      ).next();
            buf.vertex(pX1, -portalHeight, pY1).texture(2, portalHeight).color(0f, 0f, 0f, 0f).light(0      , 0      ).next();
            buf.vertex(pX1, 0            , pY1).texture(2, 0           ).color(1f, 1f, 1f, 1f).light(lightX1, lightY1).next();
        }
        buf.vertex(0, -portalHeight, 0).texture(0, 0).color(0f, 0f, 0f, 1f).light(0, 0).next();
        buf.vertex(0, -portalHeight, 1).texture(0, 0).color(0f, 0f, 0f, 1f).light(0, 0).next();
        buf.vertex(1, -portalHeight, 1).texture(0, 0).color(0f, 0f, 0f, 1f).light(0, 0).next();
        buf.vertex(1, -portalHeight, 0).texture(0, 0).color(0f, 0f, 0f, 1f).light(0, 0).next();

        tes.draw();

        GL11.glDisable(GL11.GL_STENCIL_TEST);
        
        this.lightTextureManager.disable();

        RenderSystem.popMatrix();
    }
}
