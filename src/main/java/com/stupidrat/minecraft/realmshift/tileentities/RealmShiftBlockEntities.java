package com.stupidrat.minecraft.realmshift.tileentities;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;

import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.registry.Registry;

public class RealmShiftBlockEntities {
    public static final BlockEntityType<ForgedLapisPortalEntity> FORGED_LAPIS_PORTAL = BlockEntityType.Builder.create(ForgedLapisPortalEntity::new, RealmShiftBlocks.FORGED_LAPIS_PORTAL).build(null);
    public static final BlockEntityType<RealmIgniterEntity> REALM_IGNITER_PORTAL = BlockEntityType.Builder.create(RealmIgniterEntity::new, RealmShiftBlocks.REALM_IGNITER).build(null);
    
    public static void onRegister() {
        Registry.register(Registry.BLOCK_ENTITY_TYPE, "forged_lapis_portal", FORGED_LAPIS_PORTAL);
        Registry.register(Registry.BLOCK_ENTITY_TYPE, "realm_igniter", REALM_IGNITER_PORTAL);   
    }
}
