package com.stupidrat.minecraft.realmshift.tileentities.renderer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DstFactor;
import com.mojang.blaze3d.platform.GlStateManager.SrcFactor;
import com.stupidrat.minecraft.realmshift.tileentities.RealmIgniterEntity;

import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;

public class RenderRealmIgniter extends BlockEntityRenderer<RealmIgniterEntity> {
    //public static final ResourceLocation realmIgniterTexture = new ResourceLocation(RealmShiftMod.MODID, "textures/blocks/igniter_portal.png");

    public RenderRealmIgniter(BlockEntityRenderDispatcher dispatcher) {
        super(dispatcher);
    }

    @Override
    //public void render(RealmIgniterEntity te, double x, double y, double z, float partialTicks, int destroyStage){
    public void render(RealmIgniterEntity te, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        if(!te.isActive())return;
        double x = te.getPos().getX();
        double y = te.getPos().getY();
        double z = te.getPos().getZ();
        Tessellator tes = Tessellator.getInstance();

        BufferBuilder buf = tes.getBuffer();

        GlStateManager.pushMatrix();
        GlStateManager.translated(x+0.5, y+0.5, z+0.5);
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(SrcFactor.SRC_ALPHA.field_22545, DstFactor.ONE.field_22528);
        GlStateManager.blendEquation(GL14.GL_FUNC_ADD);
        GlStateManager.disableLighting();
        this.setLightmapDisabled(true);
        //bindTexture(realmIgniterTexture);

        //240/16 is the margin of the lightmap texture
        //OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240f, 240f);

        buf.reset();
        buf.begin(GL11.GL_QUADS, VertexFormats.POSITION_TEXTURE_COLOR);
        te.drawTunnel(buf);
        //GlStateManager.cullFace(Cull.FRONT);
        tes.draw();

        buf.reset();
        buf.begin(GL11.GL_QUADS, VertexFormats.POSITION_TEXTURE_COLOR);
        te.drawTunnel(buf);
        //GlStateManager.cullFace(Cull.BACK);
        tes.draw();

        GlStateManager.popMatrix();
        GlStateManager.disableBlend();
        GlStateManager.enableLighting();
        this.setLightmapDisabled(false);
    }

    private void setLightmapDisabled(boolean b) {
        // TODO Auto-generated method stub
        
    }

    //@Override
    public boolean isGlobalRenderer(RealmIgniterEntity te)
    {
        return true;
    }
}
