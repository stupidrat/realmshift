/*
package com.stupidrat.minecraft.realmshift.sounds;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;

@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
public class RealmShiftSounds {
    public static SoundEvent REALM_IGNITER_PORTAL_AMBIENT = new SoundEvent(new ResourceLocation(RealmShiftMod.MODID, "realm_igniter_ambient")).setRegistryName("realm_igniter_ambient");
    
    @SubscribeEvent
    public static void onRegister(final RegistryEvent.Register<SoundEvent> event) {
        final IForgeRegistry<SoundEvent> registry = event.getRegistry();
        
        registry.register(REALM_IGNITER_PORTAL_AMBIENT);
    }
}
*/