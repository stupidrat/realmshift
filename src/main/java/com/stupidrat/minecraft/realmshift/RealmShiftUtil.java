package com.stupidrat.minecraft.realmshift;

public class RealmShiftUtil {

    public static double clamp(double a, double b, double x){
        if(x<a)return a;
        if(x>b)return b;
        return x;
    }

    public static double lerp(double a, double b, double x, double n) {
        double p = x/n;
        return a+(b-a)*p;
    }
    public static double bump(double a, double b, double h, double x){
        double p = (x-a)/(b-a);
        if(p<0||p>1)return 0;
        return Math.sin(p*Math.PI)*h;
    }

    public static double bump(double a, double b, double h, double x, double pow){
        return Math.pow(bump(a,b,h,x), pow);
    }

    public static double smoothstep(double x){
        if(x<0)return 0;
        if(x>1)return 1;
        return x*x*(3 - 2*x);
    }
    public static double smoothstep(double a, double b, double x){
        return a+(b-a)*smoothstep(x);
    }

    public static double smoothify(double x, double power){
        x *= power;
        if(x<-1)return -1;
        if(x> 1)return  1;
        return -x*(x*x-3)/2;
    }

    public static final int[] OFFSET_X = {1,-1, 0, 0, 0, 0};
    public static final int[] OFFSET_Y = {0, 0, 1,-1, 0, 0};
    public static final int[] OFFSET_Z = {0, 0, 0, 0, 1,-1};
}
