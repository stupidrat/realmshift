package com.stupidrat.minecraft.realmshift;

import com.stupidrat.minecraft.realmshift.tileentities.renderer.Renderers;

import net.fabricmc.api.ClientModInitializer;
import net.minecraft.client.MinecraftClient;

public class RealmShiftModClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        Renderers.registerRendering();
    }
}