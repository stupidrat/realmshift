package com.stupidrat.minecraft.realmshift.dimensions.biomes;

import net.minecraft.world.biome.Biome;

public class BiomeNemosilHills extends BiomeNemosil {
    public BiomeNemosilHills() {
        super("nemosil_hills", 120, Biome.Category.DESERT);
    }
}
