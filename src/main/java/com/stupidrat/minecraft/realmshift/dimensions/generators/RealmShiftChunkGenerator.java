package com.stupidrat.minecraft.realmshift.dimensions.generators;

import java.util.Collections;
import java.util.Optional;

import net.minecraft.world.biome.source.BiomeSource;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.chunk.StructuresConfig;

public abstract class RealmShiftChunkGenerator extends ChunkGenerator{
    public RealmShiftChunkGenerator(BiomeSource biomeSource, long seed) {
    	super(biomeSource, biomeSource, new StructuresConfig(Optional.empty(), Collections.emptyMap()), seed);
    }
    
    public abstract double getTerrainDensity(int x, int y, int z);
}