package com.stupidrat.minecraft.realmshift.dimensions.biomes;

import java.util.List;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.source.BiomeSource;

public abstract class RealmShiftBiomeSource extends BiomeSource{
    public RealmShiftBiomeSource(List<Biome> biomes) {
        super(biomes);
    }

    public abstract double getBiomesWeight(Biome biome, int x, int z);
}
