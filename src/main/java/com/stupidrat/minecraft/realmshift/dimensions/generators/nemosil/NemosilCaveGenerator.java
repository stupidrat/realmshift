/*
package com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.dimensions.biomes.RealmShiftBiomeProvider;
import com.stupidrat.minecraft.realmshift.dimensions.biomes.RealmShiftBiomes;

import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.chunk.ChunkPrimer;

public class NemosilCaveGenerator{// extends MapGenBase{
    private static final float TUNNEL_LENGTH = 10;
    private static final int TUNNEL_ELEMENTS = 148;
    private int[] tun_x = null;
    private int[] tun_y = null;
    private int[] tun_z = null;
    private float[] tun_angle = null;
    private float[] tun_slope = null;
    private World world;
    private Random rand;
    private NemosilChunkGenerator chunkGenerator;
    
    protected void recursiveGenerate(World worldIn, int chunkX, int chunkZ, int originalX, int originalZ, ChunkPrimer chunk){
        if(rand.nextInt(700)!=0)return;
        //not in plains
        RealmShiftBiomeProvider biomeProvider = (RealmShiftBiomeProvider) this.chunkGenerator.getBiomeProvider();
        double plainsWeight = biomeProvider.getBiomesWeight(RealmShiftBiomes.NEMOSIL_PLAINS, originalX * 16, originalZ * 16);
        if(plainsWeight > 0)return;
        //find a center point in the chunk
        int x = rand.nextInt(16);
        int y = 200;
        int z = rand.nextInt(16);
        //bottom of the vertical tunnel
        int y_room = rand.nextInt(30)+30;
        //prepare the tunnel element arrays
        if(tun_x == null ||
           tun_x.length != TUNNEL_ELEMENTS){
            tun_x = new int[TUNNEL_ELEMENTS];
            tun_y = new int[TUNNEL_ELEMENTS];
            tun_z = new int[TUNNEL_ELEMENTS];
            tun_angle = new float[TUNNEL_ELEMENTS];
            tun_slope = new float[TUNNEL_ELEMENTS];
        }
        AtomicInteger iter = new AtomicInteger(0);
        //generate the tunnels
        recursiveSetTunnel(iter, 1, 1, x, y_room, z, rand.nextInt(360), -rand.nextInt(3)*10, tun_x, tun_y, tun_z, tun_angle, tun_slope);
        recursiveSetTunnel(iter, 1, 1, x, y_room, z, rand.nextInt(360), -rand.nextInt(3)*10, tun_x, tun_y, tun_z, tun_angle, tun_slope);
        //relative position
        for(int i=0;i<tun_x.length;i++){
            tun_x[i] += (chunkX-originalX)*16;
            tun_z[i] += (chunkZ-originalZ)*16;
        }
        //absolute position
        x += chunkX*16;
        z += chunkZ*16;
        //find the local height
        while(this.chunkGenerator.getTerrainDensity(x, y, z) <= 0 && y > 50)y--;
        //relative position
        x -= originalX*16;
        z -= originalZ*16;
        //lip radius
        double r = 10.0 + rand.nextDouble()*4.0;
        for(int i=0;i<16;i++)
        for(int j=0;j<16;j++){
            double dist2d = Math.sqrt((i-x)*(i-x)+(j-z)*(j-z));
            //determine a factor for the margin of the chunk generation
            double margin = MathHelper.clamp((dist2d-110)*0.2, 0,1);
            for(int k=0;k<250;k++){
                //tube
                if(k<y-1-rand.nextInt(2) && k>y_room){
                    if(dist2d<5){
                        if(dist2d<2+rand.nextFloat()*3){
                            chunk.setBlockState(new BlockPos(i, k, j), Blocks.AIR.getDefaultState(), false);
                        }
                        else{
                            if((k-85)/3<rand.nextInt(5)){
                                chunk.setBlockState(new BlockPos(i, k, j), RealmShiftBlocks.NEMOSIL_STONE.getDefaultState(), false);
                            }
                            else{
                                chunk.setBlockState(new BlockPos(i, k, j), RealmShiftBlocks.NEMOSIL_ROCK.getDefaultState(), false);
                            }
                        }
                    }
                }
              //lip
                if(k>y-2*r){
                    double hyperbola = Math.abs(1.0/dist2d)*20.0;
                    double dist = Math.sqrt(dist2d*dist2d+(k-y)*(k-y)*0.4)+MathHelper.clamp(k-y, 0, 100)*0.3+hyperbola;
                    if(dist<r){
                        chunk.setBlockState(new BlockPos(i, k, j), RealmShiftBlocks.NEMOSIL_ROCK.getDefaultState(), false);
                    }
                }
                /*if(k<y_room+30){
                    //chamber
                    genChamber(chunk, i, k, j, x, y_room, z);
                }
                if(k<110){
                    for(int t=0;t<tun_x.length;t++){
                        if(genTunnel(chunk, i,k,j, tun_x[t], tun_y[t], tun_z[t], TUNNEL_LENGTH, 10, tun_angle[t], tun_slope[t], margin)){
                            break;
                        }
                    }
                }
            }
        }
    }

    private boolean genTunnel(ChunkPrimer chunk, int i, int k, int j, int x, int y, int z, double length, double thick, float angle, float slope, double margin) {
        double xp = i-x;
        double yp = k-y;
        double zp = j-z;
        //outside of possible tunnel element
        if(MathHelper.abs((int)xp)>TUNNEL_LENGTH)return false;
        if(MathHelper.abs((int)zp)>TUNNEL_LENGTH)return false;
        if(MathHelper.abs((int)yp)>TUNNEL_LENGTH)return false;
        //already empty
        if(chunk.getBlockState(new BlockPos(i, k, j)) == Blocks.AIR.getDefaultState())return true;
        float c = MathHelper.cos(angle*(float)Math.PI/180.0f);
        float s = MathHelper.sin(angle*(float)Math.PI/180.0f);
        //rotation by angle
        double xi = xp*c-zp*s;
        double zi = xp*s+zp*c;
        float tan = MathHelper.sin(slope*(float)Math.PI/180.0f)
                   /MathHelper.cos(slope*(float)Math.PI/180.0f);
        double yi = k-(y+xi*tan);
        double dist = xi*xi/(length*length) + (yi*yi+zi*zi)/thick;
        //a slight increase in diameter for fuzzy walls
        double limit = 1-margin-rand.nextDouble()*0.3;
        if(dist<limit+rand.nextDouble()*0.002){
            chunk.setBlockState(new BlockPos(i, k, j), RealmShiftBlocks.NEMOSIL_NEBLINE.getDefaultState(), false);
        }
        if(dist<limit){
            if(chunk.getBlockState(new BlockPos(i, k, j)) == Blocks.BEDROCK.getDefaultState())return true;
            chunk.setBlockState(new BlockPos(i, k, j), Blocks.AIR.getDefaultState(), false);
            return true;
        }
        return false;
    }

    private void genChamber(ChunkPrimer chunk, int px, int py, int pz, int x, int y, int z) {
        double x_room_size = 10.0 + rand.nextDouble()*10.0;
        double y_room_size = 5.0  + rand.nextDouble()*10.0;
        double z_room_size = 10.0 + rand.nextDouble()*10.0;
        double dx = (px-x)/(x_room_size);
        double dy = (py-y)/(y_room_size);
        double dz = (pz-z)/(z_room_size);
        double dist = Math.sqrt(dx*dx+dy*dy+dz*dz);
        if(dist<1.0 + rand.nextDouble()*0.2){
            chunk.setBlockState(new BlockPos(px, py, pz), Blocks.AIR.getDefaultState(), false);
        }
    }

    private void recursiveSetTunnel(AtomicInteger i, int level, int iter, int pos_x, int pos_y, int pos_z, float angle, float slope, int[] tun_x, int[] tun_y, int[] tun_z, float[] tun_angle, float[] tun_slope){
        int it = i.get();
        if(it>=tun_x.length)return;
        if(level>=5)return;
        tun_x[it] = pos_x;
        tun_y[it] = pos_y;
        tun_z[it] = pos_z;
        tun_angle[it] = -angle;
        tun_slope[it] = slope;
        double cosine = MathHelper.cos(slope*(float)Math.PI/180.0f);
        if(iter+1==4){
            for(int t=0;t<2;t++){
                i.addAndGet(1);
                float next_angle = rand.nextInt(360);
                float next_slope = rand.nextInt(3)*15-10;
                double next_x = pos_x + (MathHelper.cos(angle*(float)Math.PI/180.0f)+MathHelper.cos(next_angle*(float)Math.PI/180.0f))*TUNNEL_LENGTH*cosine*0.9;
                double next_y = pos_y + (MathHelper.sin(slope*(float)Math.PI/180.0f)+MathHelper.sin(next_slope*(float)Math.PI/180.0f))*TUNNEL_LENGTH*0.9;
                double next_z = pos_z + (MathHelper.sin(angle*(float)Math.PI/180.0f)+MathHelper.sin(next_angle*(float)Math.PI/180.0f))*TUNNEL_LENGTH*cosine*0.9;
                recursiveSetTunnel(i, level+1, 0, (int)next_x, (int)next_y, (int)next_z, next_angle, next_slope, tun_x, tun_y, tun_z, tun_angle, tun_slope);
            }
        }
        else{
            i.addAndGet(1);
            float next_angle = angle + (rand.nextInt(2)*2-1)*40;
            float next_slope = rand.nextInt(3)*15-10;
            double next_x = pos_x + (MathHelper.cos(angle*(float)Math.PI/180.0f)+MathHelper.cos(next_angle*(float)Math.PI/180.0f))*TUNNEL_LENGTH*cosine*0.9;
            double next_y = pos_y + (MathHelper.sin(slope*(float)Math.PI/180.0f)+MathHelper.sin(next_slope*(float)Math.PI/180.0f))*TUNNEL_LENGTH*0.9;
            double next_z = pos_z + (MathHelper.sin(angle*(float)Math.PI/180.0f)+MathHelper.sin(next_angle*(float)Math.PI/180.0f))*TUNNEL_LENGTH*cosine*0.9;
            recursiveSetTunnel(i, level, iter+1, (int)next_x, (int)next_y, (int)next_z, next_angle, next_slope, tun_x, tun_y, tun_z, tun_angle, tun_slope);
        }
    }
}
*/