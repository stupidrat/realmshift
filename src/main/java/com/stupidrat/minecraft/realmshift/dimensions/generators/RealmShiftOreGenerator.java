package com.stupidrat.minecraft.realmshift.dimensions.generators;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import com.stupidrat.minecraft.realmshift.RealmShiftUtil;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class RealmShiftOreGenerator{ // extends MapGenBase{
    private int[] posX = null;
    private int[] posY = null;
    private int[] posZ = null;
    private int lowCount;
    private int highCount;
    private BlockState oreBlockState;
    private int seed;
    private int lowY;
    private int highY;
    private int rarity;
    private int multiply;
    private BlockState[] replace;
    private int range;
    private Random rand;

    public RealmShiftOreGenerator(int lowY, int highY, int rarity, int multiply, int lowCount, int highCount, BlockState ore, int seed){
        this(lowY, highY, rarity, multiply, lowCount, highCount, ore, seed, (BlockState[])null);
    }

    public RealmShiftOreGenerator(int lowY, int highY, int rarity, int multiply, int lowCount, int highCount, BlockState ore, int seed, BlockState replace){
        this(lowY, highY, rarity, multiply, lowCount, highCount, ore, seed, new BlockState[]{replace});
    }

    public RealmShiftOreGenerator(int lowY, int highY, int rarity, int multiply, int lowCount, int highCount, BlockState ore, int seed, BlockState[] replace){
        this.oreBlockState = ore;
        this.lowCount = lowCount;
        this.highCount = highCount;
        this.range = 0;
        this.seed = seed;
        this.lowY = lowY;
        this.highY = highY;
        this.rarity = rarity;
        this.multiply = multiply;
        this.replace = replace;
    }

    //@Override
    protected void recursiveGenerate(World worldIn, int chunkX, int chunkZ, int originalX, int originalZ, Chunk chunk){
        //must do this to change distribution seed
        for(int i=0; i<this.seed; i++)rand.nextInt();
        if(rand.nextInt(this.rarity)!=0)return;

        int oreNumber = rand.nextInt(highCount-lowCount+1)+lowCount;

        if(posX == null){
            posX = new int[highCount];
            posY = new int[highCount];
            posZ = new int[highCount];
        }

        AtomicInteger iter = new AtomicInteger(0);
        for(int mult=0;mult<multiply;mult++){
            //find a center point in the chunk
            int x = rand.nextInt(16);
            int y = rand.nextInt(highY-lowY)+lowY;
            int z = rand.nextInt(16);
            iter.set(0);
            recurseAndPlace(rand, iter, oreNumber, x, y, z, chunk);
            for(int i=0;i<iter.get();i++){
                putBlock(posX[i], posY[i], posZ[i], chunk);
            }
        }
    }

    private void recurseAndPlace(Random rand, AtomicInteger iter, int oreNumber, int x, int y, int z, Chunk chunk) {
        int i = iter.get();
        if(i>=oreNumber)return;
        boolean canPlace = canPutBlock(i, x, y, z, chunk);
        if(canPlace){
            posX[i] = x;
            posY[i] = y;
            posZ[i] = z;
            iter.addAndGet(1);
            for(int t=0;t<6;t++){
                int dir = rand.nextInt(6);
                recurseAndPlace(rand, iter, oreNumber, x+RealmShiftUtil.OFFSET_X[dir], y+RealmShiftUtil.OFFSET_Y[dir], z+RealmShiftUtil.OFFSET_Z[dir], chunk);
                if(iter.get()>=oreNumber)return;
            }
        }
    }
    private void putBlock(int x, int y, int z, Chunk chunk){
        if(x<0)return;
        if(y<0)return;
        if(z<0)return;
        if(x>=16)return;
        if(y>=256)return;
        if(z>=16)return;
        if(!canReplace(x, y, z, chunk))return;
        chunk.setBlockState(new BlockPos(x, y, z), oreBlockState, false);
    }

    private boolean canReplace(int x, int y, int z, Chunk chunk) {
        if(this.replace == null)return true;
        for(BlockState candidate : this.replace){
            if(chunk.getBlockState(new BlockPos(x, y, z)) == candidate)return true;
        }
        return false;
    }

    //returns false if there cannot be placed a block
    private boolean canPutBlock(int n, int x, int y, int z, Chunk chunk){
        for(int i=0;i<n;i++){
            if(posX[i] == x &&
               posY[i] == y &&
               posZ[i] == z){
                return false;
            }
        }
        return canPutBlockInPos(x, y, z, chunk);
    }

    private boolean canPutBlockInPos(int x, int y, int z, Chunk chunk){
        if(x<0)return false;
        if(y<0)return false;
        if(z<0)return false;
        if(x>=16)return false;
        if(y>=256)return false;
        if(z>=16)return false;
        if(chunk.getBlockState(new BlockPos(x, y, z)) == Blocks.AIR.getDefaultState())return false;
        if(chunk.getBlockState(new BlockPos(x, y, z)) == oreBlockState)return false;
        return true;
    }
}
