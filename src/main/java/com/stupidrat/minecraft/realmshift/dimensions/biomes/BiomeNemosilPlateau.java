package com.stupidrat.minecraft.realmshift.dimensions.biomes;

import net.minecraft.world.biome.Biome;

public class BiomeNemosilPlateau extends BiomeNemosil {
    public BiomeNemosilPlateau() {
        super("nemosil_plateau", 140, Biome.Category.DESERT);
    }
}
