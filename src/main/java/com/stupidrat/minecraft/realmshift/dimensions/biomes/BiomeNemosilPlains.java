package com.stupidrat.minecraft.realmshift.dimensions.biomes;

import net.minecraft.world.biome.Biome;

public class BiomeNemosilPlains extends BiomeNemosil {
    public BiomeNemosilPlains() {
        super("nemosil_plains", 90, Biome.Category.DESERT);
    }
}
