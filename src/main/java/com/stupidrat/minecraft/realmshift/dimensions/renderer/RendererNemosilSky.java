/*
package com.stupidrat.minecraft.realmshift.dimensions.renderer;

import java.util.Random;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;

import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;
import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.IRenderHandler;

public class RendererNemosilSky implements IRenderHandler{
    public static final ResourceLocation GAS_GIANT = new ResourceLocation(RealmShiftMod.MODID, "textures/env/gas_giant.png");
    private static VertexBuffer starVBO = null;

    @OnlyIn(Dist.CLIENT)
    public void initStars(){
        if(starVBO != null)return;

        Tessellator tes = Tessellator.getInstance();
        BufferBuilder buf = tes.getBuffer();

        boolean vboEnabled = GLX.useVbo();
        if(vboEnabled){
            RendererNemosilSky.starVBO = new VertexBuffer(DefaultVertexFormats.POSITION_TEX);
            buf.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
            this.renderStars(buf);
            buf.finishDrawing();
            buf.reset();
            RendererNemosilSky.starVBO.bufferData(buf.getByteBuffer());
        }
        else{
            System.err.println("VBOs are not enabled!");
        }
    }

    private void renderStars(BufferBuilder buf) {
        Random rnd = new Random(1);
        for(int i=0;i<1000;i++) renderStar(buf, (float) (rnd.nextFloat()*0.03+0.03), 2, rnd);
        for(int i=0;i<100;i++) renderStar(buf, (float) (rnd.nextFloat()*0.03+0.03), 1, rnd);
        for(int i=0;i<30;i++) renderStar(buf, (float) (rnd.nextFloat()*0.03+0.03), 0, rnd);
    }

    private void renderStar(BufferBuilder buf, float size, int type, Random rnd) {
        float x = rnd.nextFloat()*2.0f-1.0f;
        float y = rnd.nextFloat()*2.0f-1.0f;
        y*=y*y;
        float z = rnd.nextFloat()*2.0f-1.0f;
        float l = (float) Math.sqrt(x*x+y*y+z*z);
        x /= l;
        y /= l;
        z /= l;

        float xozAngle = (float) Math.atan2(z, x);
        float ux = (float) Math.cos(xozAngle+Math.PI/2f)*size;
        float uy = 0;
        float uz = (float) Math.sin(xozAngle+Math.PI/2f)*size;

        float xoyAngle = (float) Math.atan2(y, Math.sqrt(x*x+z*z));
        float px = (float) Math.cos(xoyAngle+Math.PI/2f);
        float vx = (float) Math.cos(xozAngle)*px*size;
        float vy = (float) Math.sin(xoyAngle+Math.PI/2f)*size;
        float vz = (float) Math.sin(xozAngle)*px*size;

        float angle = rnd.nextFloat()*(float)Math.PI*2f;

        for(int j=0;j<4;j++){
            float c = (float)Math.cos(angle+j*Math.PI/2f);
            float s = (float)Math.sin(angle+j*Math.PI/2f);
            int u = (j&2)/2;
            int v = ((j+1)&2)/2;
            buf.pos(x+ux*c+vx*s,
                    y+uy*c+vy*s,
                    z+uz*c+vz*s)
               .tex(u*0.125, v*0.125+type*0.125)
               .endVertex();
        }
    }

    @Override
    public void render(int ticks, float partialTicks, ClientWorld world, Minecraft mc) {
        float timeSkyAngle = world.getCelestialAngle(partialTicks);
        float starSkyAngle = getStarAngle(world.getGameTime(), partialTicks);
        float gasGiantAngle = getGasGiantAngle(world.getGameTime(), partialTicks);
        TextureManager tex = mc.getTextureManager();

        Tessellator tes = Tessellator.getInstance();
        BufferBuilder buf = tes.getBuffer();

        GlStateManager.pushMatrix();
        GlStateManager.disableCull();
        GlStateManager.depthMask(false);
        tex.bindTexture(GAS_GIANT);
        GlStateManager.disableFog();

        //translate to head
        GlStateManager.translated(0, 1.65,0);
        GlStateManager.rotated(30, 0, 0, 1);
        GlStateManager.scaled(100, 100, 100);
        GlStateManager.pushMatrix();
        GlStateManager.rotated(starSkyAngle*360, 1, 0, 0);

        GlStateManager.enableBlend();
        GlStateManager.blendFunc(SourceFactor.ONE, DestFactor.ONE);
        GlStateManager.blendEquation(GL14.GL_FUNC_ADD);

        initStars();
        RendererNemosilSky.starVBO.bindBuffer();
        GlStateManager.enableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.enableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.vertexPointer(3, GL11.GL_FLOAT, 3*4+2*4, 0);
        GlStateManager.texCoordPointer(2, GL11.GL_FLOAT, 3*4+2*4, 3*4);
        RendererNemosilSky.starVBO.drawArrays(GL11.GL_QUADS);
        VertexBuffer.unbindBuffer();
        GlStateManager.disableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.disableClientState(GL11.GL_TEXTURE_COORD_ARRAY);

        /*buf.reset();
        buf.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        renderStars(buf);
        tes.draw();

        GlStateManager.disableBlend();

        GlStateManager.popMatrix();

        GlStateManager.rotated(timeSkyAngle*360, 1, 0, 0);

        drawGasGiant(buf, 0.3f, gasGiantAngle);

        GlStateManager.enableFog();
        GlStateManager.disableBlend();
        GlStateManager.enableCull();
        GlStateManager.depthMask(true);
        GlStateManager.popMatrix();
    }

    private void drawGasGiant(BufferBuilder buf, float radius, float gasGiantAngle) {
        Tessellator tes = Tessellator.getInstance();
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(SourceFactor.ONE, DestFactor.ONE);
        GlStateManager.blendEquation(GL14.GL_FUNC_ADD);
        //glow
        buf.reset();
        buf.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        buf.pos( 2*radius, 1, -2*radius).tex(0.25, 0.0).endVertex();
        buf.pos(-2*radius, 1, -2*radius).tex(0.25, 0.75).endVertex();
        buf.pos(-2*radius, 1,  2*radius).tex(1.0, 0.75).endVertex();
        buf.pos( 2*radius, 1,  2*radius).tex(1.0, 0.0).endVertex();
        tes.draw();

        GlStateManager.disableBlend();
        //planet
        buf.reset();
        buf.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        buf.pos( radius, 1,  radius).tex(0+gasGiantAngle, 0.75).endVertex();
        buf.pos( radius, 1, -radius).tex(0+gasGiantAngle, 1.0).endVertex();
        buf.pos(-radius, 1, -radius).tex(0.25+gasGiantAngle, 1.0).endVertex();
        buf.pos(-radius, 1,  radius).tex(0.25+gasGiantAngle, 0.75).endVertex();
        tes.draw();

        GlStateManager.enableBlend();
        GlStateManager.blendFunc(SourceFactor.DST_COLOR, DestFactor.ZERO);

        //shade
        buf.reset();
        buf.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
        buf.pos(-radius, 1, -radius).tex(0.25, 0.75).endVertex();
        buf.pos(-radius, 1,  radius).tex(0.25, 0.5).endVertex();
        buf.pos( radius, 1,  radius).tex(0, 0.5).endVertex();
        buf.pos( radius, 1, -radius).tex(0, 0.75).endVertex();
        tes.draw();
        GlStateManager.disableBlend();
    }

    private float getStarAngle(long worldTime, float partialTicks) {
        int i = (int)(worldTime % 50000L);
        float f = (i + partialTicks) / 50000.0F;

        if (f < 0.0F)
        {
            ++f;
        }

        if (f > 1.0F)
        {
            --f;
        }

        return f;
    }

    private float getGasGiantAngle(long worldTime, float partialTicks) {
        int i = (int)(worldTime % 10000L);
        float f = (i + partialTicks) / 10000.0F;

        if (f < 0.0F)
        {
            ++f;
        }

        if (f > 1.0F)
        {
            --f;
        }

        return f;
    }
}
*/