package com.stupidrat.minecraft.realmshift.dimensions.biomes;

import net.minecraft.sound.BiomeMoodSound;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeEffects;

public class BiomeNemosil extends RealmShiftBiome {
    public BiomeNemosil(String registryName, int depth, Category category) {
        super(registryName, category, (new Biome.Settings()).precipitation(Precipitation.NONE)
                                                .category(Category.DESERT)
                                                .scale(1.0f)
                                                .temperature(2.0f)
                                                .downfall(0.0f)
                                                .depth(depth)
                                                .effects((new BiomeEffects.Builder()).waterColor(0x008080)
                                                                                     .waterFogColor(0x008080)
                                                                                     .fogColor(0x008080)
                                                                                     .moodSound(BiomeMoodSound.CAVE)
                                                                                     .build())
                                                .parent("desert")
                                                  /*
                                                  .(0x008080)
                                                  .waterFogColor(0x008080)
                                                  */
             );
    }
}
