package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlock;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.stat.Stats;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class NemosilCobble extends RealmShiftBlock {
    public NemosilCobble() {
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.PURPLE)
                .strength(3, 10)
                .sounds(BlockSoundGroup.STONE)
                .breakByTool(FabricToolTags.PICKAXES),
                "nemosil_cobble");
    }

    @Override
    public void afterBreak(World world, PlayerEntity player, BlockPos pos, BlockState state, BlockEntity blockEntity, ItemStack stack) {
    	player.incrementStat(Stats.MINED.getOrCreateStat(this));
        player.addExhaustion(0.005F);
        
        if(world instanceof ServerWorld) {	
	        if (EnchantmentHelper.getEquipmentLevel(Enchantments.SILK_TOUCH, player) > 0)
	        {
	            List<ItemStack> items = new ArrayList<ItemStack>();
	            ItemStack itemstack = this.getPickStack(world, pos, state);
	
	            if (!itemstack.isEmpty())
	            {
	                items.add(itemstack);
	            }
	
	            for (ItemStack item : items)
	            {
	            	dropStack(world, pos, item);
	            }
	        }
	        else
	        {
	            int fortune = EnchantmentHelper.getEquipmentLevel(Enchantments.FORTUNE, player);
	            Item item = stack.getItem();
	            int level = 0;
	            if(item instanceof MiningToolItem) {
	            	MiningToolItem miningTool = (MiningToolItem)item;
	            	level = miningTool.getMaterial().getMiningLevel();
	            }
	            this.dropBlockAsItemBasedOnToolLevel(world, pos, state, 1.0F, fortune, level);
	        }
        }
    }

    public void dropBlockAsItemBasedOnToolLevel(World worldIn, BlockPos pos, BlockState state, float chance, int fortune, int toollevel)
    {
        Random rand = worldIn.random;
	
        List<ItemStack> drops = new ArrayList<ItemStack>();
        System.out.println(toollevel);
        double chanceForIron = 0.12+fortune*0.07+toollevel*0.07;
        System.out.println(chanceForIron);
        //wood
        if(toollevel<=0){
            drops.add(new ItemStack(Blocks.COBBLESTONE, 1));
        }else{
            if(rand.nextDouble()<chanceForIron){//stone and above
                drops.add(new ItemStack(Blocks.IRON_ORE, 1));
            }else{
                drops.add(new ItemStack(Blocks.COBBLESTONE, 1));
            }
        }

        for (ItemStack drop : drops)
        {
            if (worldIn.random.nextFloat() <= chance)
            {
                dropStack(worldIn, pos, drop);
            }
        }
    }
}
