package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import java.util.Random;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlock;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.math.BlockPos;

public class NemosilGrass extends RealmShiftBlock {
    public NemosilGrass() {
        super(FabricBlockSettings.of(Material.SOIL, MaterialColor.ORANGE)
                .strength(0.5f, 4)
                .sounds(BlockSoundGroup.GRASS)
                .breakByTool(FabricToolTags.SHOVELS)
                .ticksRandomly(),
                "nemosil_grass");
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random rand)
    {
        if(world.getBlockState(pos.up()).isFullCube(world, pos.up()) &&
           world.getBlockState(pos.up()).isOpaque()){
            world.setBlockState(pos, RealmShiftBlocks.NEMOSIL_DIRT.getDefaultState());
        }
    }
}
