package com.stupidrat.minecraft.realmshift.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldView;

public interface IPlantable {
    public boolean isValidPosition(BlockState state, WorldView worldIn, BlockPos pos);
}
