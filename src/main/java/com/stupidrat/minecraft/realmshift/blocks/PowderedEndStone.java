package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;

public class PowderedEndStone extends RealmShiftBlock{
    public PowderedEndStone(){
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.SAND).strength(4, 15), "powdered_end_stone");
    }
}
