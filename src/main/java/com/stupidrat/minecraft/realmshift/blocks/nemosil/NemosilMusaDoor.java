package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftDoor;

public class NemosilMusaDoor extends RealmShiftDoor {
    public NemosilMusaDoor() {
        super("nemosil_musa_door");
    }
}
