package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.Direction;

public class RealmShiftLog extends RealmShiftBlock{

    public RealmShiftLog(String name) {
        super(FabricBlockSettings.of(Material.WOOD, MaterialColor.WOOD).strength(2, 7).sounds(BlockSoundGroup.WOOD), name);
        this.setDefaultState(this.getDefaultState().with(AXIS, Direction.Axis.Y));
        
        registerFlammability(5, 5);
    }

    public static final EnumProperty<Direction.Axis> AXIS = Properties.AXIS;

    public BlockState getPlacementState(ItemPlacementContext ctx) {
        return (BlockState)this.getDefaultState().with(AXIS, ctx.getSide().getAxis());
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager) {
    	stateManager.add(AXIS);
    }
}
