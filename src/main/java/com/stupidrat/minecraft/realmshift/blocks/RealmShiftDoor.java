package com.stupidrat.minecraft.realmshift.blocks;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.ItemGroup;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;

public class RealmShiftDoor extends DoorBlock implements IRealmShiftBlock, IGroupedBlock{
	public String name;

	public RealmShiftDoor(String name) {
        super(FabricBlockSettings.of(Material.WOOD, MaterialColor.WOOD).hardness(3).sounds(BlockSoundGroup.WOOD));
        this.name = name;
    }
    
    public ItemGroup getGroup() {
        return ItemGroup.REDSTONE;
    }
    
	@Override
	public Identifier getIdentifier() {
		return new Identifier(RealmShiftMod.MODID, this.name);
	}

	@Override
	public RenderLayer getRenderLayer() {
		return RenderLayer.getCutoutMipped();
	}
}
