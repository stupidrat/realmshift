package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;

public class ForgedEndStone extends RealmShiftBlock{
    public ForgedEndStone(){
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.SAND).strength(4, 15), "forged_end_stone");
    }
}
