package com.stupidrat.minecraft.realmshift.blocks;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.util.Identifier;

public interface IRealmShiftBlock{
	public Identifier getIdentifier();
    public RenderLayer getRenderLayer();
}
