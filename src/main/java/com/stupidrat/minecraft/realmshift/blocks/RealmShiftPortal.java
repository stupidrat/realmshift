package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;

public class RealmShiftPortal extends RealmShiftBlock{
    public RealmShiftPortal(FabricBlockSettings fabricBlockSettings, String name){
        super(fabricBlockSettings.strength(-1, 100000)
                                 .noCollision(),
              name);
    }

    @Override
    public int getOpacity(BlockState state, BlockView view, BlockPos pos) {
        return 0;
    }

    @Override
    public BlockRenderType getRenderType(BlockState state)
    {
        return BlockRenderType.INVISIBLE;
    }
}
