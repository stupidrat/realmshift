package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;

public class RealmShiftPlant extends RealmShiftBlock implements IPlantable {
    public BlockState[] placeableOn;

    public RealmShiftPlant(FabricBlockSettings settings, String name, BlockState[] placeableOn, RenderType renderType) {
        super(settings, name);

        this.placeableOn = placeableOn;
        this.renderType = renderType;
    }

    public ItemGroup getGroup() {
        return ItemGroup.DECORATIONS;
    }

    public static final VoxelShape SAPLING_SHAPE = Block.createCuboidShape(2.0D, 0.0D, 2.0D, 14.0D, 12.0D, 14.0D);

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return SAPLING_SHAPE;
    }

    @Override
    public Block.OffsetType getOffsetType() {
        return Block.OffsetType.XYZ;
    }
    
    @Override
    public void neighborUpdate(BlockState state, World world, BlockPos pos, Block block, BlockPos neighborPos, boolean moved) {
        if (world.isAir(pos.down()))
        {
            world.breakBlock(pos, false);
        }
    }

    @Override
    public boolean canPlaceAt(BlockState state, WorldView world, BlockPos pos)
    {
    	return isValidPosition(state, world, pos);
    }

    @Override
    public boolean isValidPosition(BlockState state, WorldView worldIn, BlockPos pos) {
        BlockState under = worldIn.getBlockState(pos.down());
        boolean canPlace = false;
        if(placeableOn != null) {
        	for(BlockState i : placeableOn){
        		if(i == under)canPlace = true;
        	}
        }
        else {
        	canPlace = true;
        }
        return canPlace && worldIn.getBlockState(pos).getMaterial().isReplaceable();
	}
}
