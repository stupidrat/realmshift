package com.stupidrat.minecraft.realmshift.blocks;import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;import net.minecraft.block.Material;import net.minecraft.block.MaterialColor;public class RealmIgniterPortal extends RealmShiftPortal{
    public RealmIgniterPortal(){
        super(FabricBlockSettings.of(Material.PORTAL,  MaterialColor.BLACK).lightLevel(10), "realm_igniter_portal");
    }
}
