package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import java.util.Random;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlock;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.math.BlockPos;

public class NemosilDirt extends RealmShiftBlock {
    public NemosilDirt() {
        super(FabricBlockSettings.of(Material.SOIL, MaterialColor.GREEN)
                .strength(0.5f, 4)
                .sounds(BlockSoundGroup.GRAVEL)
                .breakByTool(FabricToolTags.SHOVELS)
                .ticksRandomly(),
                "nemosil_dirt");
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random rand)
    {
        for(int x=-1;x<=1;x++)
        for(int y=-1;y<=1;y++)
        for(int z=-1;z<=1;z++){
            if(x!=0 || z!=0)
            if(!world.getBlockState(pos.up()).getMaterial().isSolid() &&
               world.getBlockState(pos.add(x, y, z)).getBlock() == RealmShiftBlocks.NEMOSIL_GRASS){
                world.setBlockState(pos, RealmShiftBlocks.NEMOSIL_GRASS.getDefaultState());
                return;
            }
        }
    }
}
