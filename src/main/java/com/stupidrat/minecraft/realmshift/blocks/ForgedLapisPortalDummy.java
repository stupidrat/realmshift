package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.block.ShapeContext;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class ForgedLapisPortalDummy extends RealmShiftPortal{
    public ForgedLapisPortalDummy(){
        super(FabricBlockSettings.of(Material.PORTAL, MaterialColor.BLACK), "forged_lapis_portal_dummy");
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return ForgedLapisPortal.LAPIS_PORTAL_SHAPE;
    }

    @Override
    public void onEntityCollision(BlockState state, World worldIn, BlockPos pos, Entity entityIn)
    {
        RealmShiftBlocks.FORGED_LAPIS_PORTAL.onEntityCollision(state, worldIn, pos, entityIn);
    }
}
