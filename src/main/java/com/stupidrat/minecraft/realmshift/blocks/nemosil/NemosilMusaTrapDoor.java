package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftTrapDoor;

public class NemosilMusaTrapDoor extends RealmShiftTrapDoor {
    public NemosilMusaTrapDoor() {
        super("nemosil_musa_trapdoor");
    }
}
