package com.stupidrat.minecraft.realmshift.blocks;

import net.minecraft.item.ItemGroup;

public interface IGroupedBlock {
    public ItemGroup getGroup();
}
