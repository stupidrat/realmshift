package com.stupidrat.minecraft.realmshift.proxy;

import net.minecraft.item.Item;

public abstract class CommonProxy{
	public abstract void registerItemRenderer(Item item);

    public abstract void registerItemRendererWithMeta(Item item, int meta, String resource);

    public abstract void enableStencil();
}
