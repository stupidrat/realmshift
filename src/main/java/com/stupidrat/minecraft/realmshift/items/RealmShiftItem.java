package com.stupidrat.minecraft.realmshift.items;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.minecraft.item.Item;
import net.minecraft.util.Identifier;

public class RealmShiftItem extends Item implements IRealmShiftItem{
    public String name;

    public RealmShiftItem(Item.Settings settings, String name){
        super(settings);
        this.name = name;
    }
    
    public Identifier getIdentifier() {
    	return new Identifier(RealmShiftMod.MODID, this.name);
    }
}
