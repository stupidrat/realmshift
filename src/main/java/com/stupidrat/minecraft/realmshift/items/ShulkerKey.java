package com.stupidrat.minecraft.realmshift.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

public class ShulkerKey extends RealmShiftItem {
    public ShulkerKey(){
        super(new Item.Settings().maxCount(1).group(ItemGroup.MISC), "shulker_key");
    }
}
