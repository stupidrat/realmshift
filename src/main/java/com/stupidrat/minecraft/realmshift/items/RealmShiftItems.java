package com.stupidrat.minecraft.realmshift.items;

import com.stupidrat.minecraft.realmshift.items.nemosil.NeblineArmorBody;
import com.stupidrat.minecraft.realmshift.items.nemosil.NeblineArmorFeet;
import com.stupidrat.minecraft.realmshift.items.nemosil.NeblineArmorHead;
import com.stupidrat.minecraft.realmshift.items.nemosil.NeblineArmorLegs;
import com.stupidrat.minecraft.realmshift.items.nemosil.NeblineShard;

import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

public class RealmShiftItems {
    public static final ShulkerKey SHULKER_KEY = new ShulkerKey();

    public static final NeblineShard NEMOSIL_NEBLINE_SHARD = new NeblineShard();

    public static final NeblineArmorHead NEMOSIL_NEBLINE_ARMOR_HEAD = new NeblineArmorHead();
    public static final NeblineArmorBody NEMOSIL_NEBLINE_ARMOR_BODY = new NeblineArmorBody();
    public static final NeblineArmorLegs NEMOSIL_NEBLINE_ARMOR_LEGS = new NeblineArmorLegs();
    public static final NeblineArmorFeet NEMOSIL_NEBLINE_ARMOR_FEET = new NeblineArmorFeet();

    public static void registerItems() {
        final Item[] items = {
            SHULKER_KEY,
            NEMOSIL_NEBLINE_SHARD,
            NEMOSIL_NEBLINE_ARMOR_HEAD,
            NEMOSIL_NEBLINE_ARMOR_BODY,
            NEMOSIL_NEBLINE_ARMOR_LEGS,
            NEMOSIL_NEBLINE_ARMOR_FEET
        };

        for (final Item item : items) {
            Registry.register(Registry.ITEM, ((IRealmShiftItem)item).getIdentifier(), item);
        }
    }
}
