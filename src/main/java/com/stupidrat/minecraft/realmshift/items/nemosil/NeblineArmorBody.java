
package com.stupidrat.minecraft.realmshift.items.nemosil;

import com.stupidrat.minecraft.realmshift.items.RealmShiftArmor;
import com.stupidrat.minecraft.realmshift.items.RealmShiftArmorMaterial;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;

public class NeblineArmorBody extends RealmShiftArmor {
    public NeblineArmorBody(){
        super("nemosil_nebline_armor_body", RealmShiftArmorMaterial.NEBLINE, EquipmentSlot.CHEST, (new Item.Settings()).maxCount(1));
    }
}
