package com.stupidrat.minecraft.realmshift.items;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;

public class RealmShiftArmor extends ArmorItem implements IRealmShiftItem {
    public String name;

    public RealmShiftArmor(String name, RealmShiftArmorMaterial materialIn, EquipmentSlot slot, Item.Settings settings) {
        super(materialIn, slot, (new Item.Settings()).group(ItemGroup.COMBAT).maxCount(1));
        this.name = name;
    }
    
    public Identifier getIdentifier() {
    	return new Identifier(RealmShiftMod.MODID, this.name);
    }
}
