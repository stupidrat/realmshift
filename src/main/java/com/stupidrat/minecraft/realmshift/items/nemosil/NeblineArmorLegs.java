
package com.stupidrat.minecraft.realmshift.items.nemosil;

import com.stupidrat.minecraft.realmshift.items.RealmShiftArmor;
import com.stupidrat.minecraft.realmshift.items.RealmShiftArmorMaterial;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;

public class NeblineArmorLegs extends RealmShiftArmor {
    public NeblineArmorLegs(){
        super("nemosil_nebline_armor_legs", RealmShiftArmorMaterial.NEBLINE, EquipmentSlot.LEGS, (new Item.Settings()).maxCount(1));
    }
}
