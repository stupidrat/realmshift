package com.stupidrat.minecraft.realmshift.items;

import java.util.function.Supplier;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;

public enum RealmShiftArmorMaterial implements ArmorMaterial {
   NEBLINE("nebline", 33, new int[]{3, 6, 8, 3}, 9, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 1.0F, 1.0F, () -> {
      return Ingredient.ofItems(RealmShiftItems.NEMOSIL_NEBLINE_SHARD);
   });

   private static final int[] MAX_DAMAGE_ARRAY = new int[]{13, 15, 16, 11};
   private final String name;
   private final int maxDamageFactor;
   private final int[] damageReductionAmountArray;
   private final int enchantability;
   private final SoundEvent soundEvent;
   private final float toughness;
   private final Ingredient repairMaterial;
   private final float knockBackResistance;

   private RealmShiftArmorMaterial(String nameIn, int maxDamageFactorIn, int[] damageReductionAmountsIn, int enchantabilityIn, SoundEvent equipSoundIn, float toughness, float knockBackResistance, Supplier<Ingredient> repairMaterialSupplier) {
      this.name = nameIn;
      this.maxDamageFactor = maxDamageFactorIn;
      this.damageReductionAmountArray = damageReductionAmountsIn;
      this.enchantability = enchantabilityIn;
      this.soundEvent = equipSoundIn;
      this.toughness = toughness;
      this.knockBackResistance = knockBackResistance;
      this.repairMaterial = repairMaterialSupplier.get();
   }

   @Override
   public int getDurability(EquipmentSlot slotIn) {
	   return MAX_DAMAGE_ARRAY[slotIn.getEntitySlotId()] * this.maxDamageFactor;
   }

   public int getProtectionAmount(EquipmentSlot slot) {
      return this.damageReductionAmountArray[slot.getEntitySlotId()];
   }

   public int getEnchantability() {
      return this.enchantability;
   }

   @Override
   public SoundEvent getEquipSound() {
      return this.soundEvent;
   }

   @Override
   public Ingredient getRepairIngredient() {
	   return this.repairMaterial;
   }

   public String getName() {
      return this.name;
   }

   public float getToughness() {
      return this.toughness;
   }

	@Override
	public float getKnockbackResistance() {
		return this.knockBackResistance;
	}
}
