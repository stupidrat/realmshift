
package com.stupidrat.minecraft.realmshift.items.nemosil;

import com.stupidrat.minecraft.realmshift.items.RealmShiftArmor;
import com.stupidrat.minecraft.realmshift.items.RealmShiftArmorMaterial;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;

public class NeblineArmorHead extends RealmShiftArmor {
    public NeblineArmorHead(){
        super("nemosil_nebline_armor_head", RealmShiftArmorMaterial.NEBLINE, EquipmentSlot.HEAD, (new Item.Settings()).maxCount(1));
    }
}
